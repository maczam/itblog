package me.itblog;

import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import me.itblog.bean.Article;
import me.itblog.bean.Category;
import me.itblog.utils.Constant;
import org.nutz.dao.Cnd;
import org.nutz.lang.Strings;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/12/19.
 */
public class TestRome1 {
    public static void main(String[] args) throws Exception {
        URL feedUrl = new URL("http://it.deepinmind.com/atom.xml");
        SyndFeedInput input = new SyndFeedInput();
        input.setAllowDoctypes(true);
        URLConnection urlConnection = feedUrl.openConnection();
        urlConnection.setConnectTimeout(Constant.fetch_connect_time_out);
        urlConnection.setReadTimeout(Constant.fetch_read_time_out);
        SyndFeed feed = input.build(new XmlReader(urlConnection));
        // 得到Rss新闻中子项列表
        List entries = feed.getEntries();
        // 循环得到每个子项信息
        System.out.println("开始抓取authorProfile>>" + "一共抓到size>>>" + entries.size());

        for (int i = 0; i < entries.size(); i++) {
            SyndEntry entry = (SyndEntry) entries.get(i);
            String link = entry.getLink();
            Article article = new Article();
            System.out.println("开始抓取authorProfile>>" + "正在保存>>>" + i + "   :" + entry.getTitle());
            // 标题、连接地址、标题简介、时间是一个Rss源项最基本的组成部分
            article.setTitle(entry.getTitle());
            article.setLink(entry.getLink());

            SyndContent description = entry.getDescription();
            String value = description.getValue();

            article.setPublishedDate(entry.getPublishedDate());

            article.setAuthorName(entry.getAuthor());

            List<SyndContent> contents = entry.getContents();
            for (SyndContent content : contents) {
                article.setContents(content.getValue());
            }


            // 此标题所属的范畴
            List categoryList = entry.getCategories();
            List<Category> categories = new ArrayList<Category>();
            StringBuilder sb = new StringBuilder();
            if (categoryList != null) {
                for (int m = 0; m < categoryList.size(); m++) {
                    SyndCategory category = (SyndCategory) categoryList.get(m);
                    String name = category.getName();

                }
            }
            if (categories.size() > 0) {
                article.setCategoryList(categories);
                System.out.println("categories>>>>" + sb.toString().length());
                article.setCategorys(sb.toString());
            }
        }
    }
}

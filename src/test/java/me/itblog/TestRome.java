package me.itblog;

import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEnclosure;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.junit.Test;

import java.net.URL;
import java.util.List;

/**
 * Created by infi.he on 2015/11/23.
 */
public class TestRome {

    @Test
    public void test1() {
        try {
//            URL feedUrl = new URL("http://www.zhangxinxu.com/wordpress/feed/");
//            URL feedUrl = new URL("http://www.mongoing.com/feed");
            URL feedUrl = new URL("http://www.blogchong.com/feed.asp");

            SyndFeedInput input = new SyndFeedInput();
            input.setAllowDoctypes(true);
            SyndFeed feed = input.build(new XmlReader(feedUrl));
            // 得到Rss新闻中子项列表
            List entries = feed.getEntries();
            // 循环得到每个子项信息
            for (int i = 0; i < entries.size(); i++) {
                System.out.println("-------------------------------------------------------------------------------------");
                SyndEntry entry = (SyndEntry) entries.get(i);
                // 标题、连接地址、标题简介、时间是一个Rss源项最基本的组成部分
                System.out.println("标题：" + entry.getTitle());
                System.out.println("连接地址：" + entry.getLink());
                SyndContent description = entry.getDescription();
                System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                System.out.println("标题简介：" + description.getValue());
                System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");

                System.out.println("发布时间：" + entry.getPublishedDate());
                // 以下是Rss源可先的几个部分
                System.out.println("标题的作者：" + entry.getAuthor());
                List<SyndContent> contents = entry.getContents();
                System.out.println("=========================================================================================");
                for (SyndContent content : contents) {
                    System.out.println(content.getValue().substring(0,200));
                }
                System.out.println("=========================================================================================");

                // 此标题所属的范畴
                List categoryList = entry.getCategories();
                System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                if (categoryList != null) {
                    for (int m = 0; m < categoryList.size(); m++) {
                        SyndCategory category = (SyndCategory) categoryList.get(m);
                        System.out.println("此标题所属的范畴：" + category.getName());
                    }
                }
                System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

                // 得到流媒体播放文件的信息列表
                List enclosureList = entry.getEnclosures();
                if (enclosureList != null) {
                    for (int n = 0; n < enclosureList.size(); n++) {
                        SyndEnclosure enclosure = (SyndEnclosure) enclosureList.get(n);
                        System.out.println("流媒体播放文件：" + entry.getEnclosures());
                    }
                }
                System.out.println();
            }

        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.getMessage());
        }

    }


    @Test
    public void test2(){
        try {
        URL feedUrl = new URL("http://www.infoq.com/cn/feed");

        SyndFeedInput input = new SyndFeedInput();
        input.setAllowDoctypes(true);
        SyndFeed feed = input.build(new XmlReader(feedUrl));
        // 得到Rss新闻中子项列表
        List entries = feed.getEntries();
        // 循环得到每个子项信息
        for (int i = 0; i < entries.size(); i++) {
            System.out.println("-------------------------------------------------------------------------------------");
            SyndEntry entry = (SyndEntry) entries.get(i);
            // 标题、连接地址、标题简介、时间是一个Rss源项最基本的组成部分
            System.out.println("标题：" + entry.getTitle());
            System.out.println("连接地址：" + entry.getLink());
            SyndContent description = entry.getDescription();
            System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
            System.out.println("标题简介：" + description.getValue());
            System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");

            System.out.println("发布时间：" + entry.getPublishedDate());
            // 以下是Rss源可先的几个部分
            System.out.println("标题的作者：" + entry.getAuthor());
            List<SyndContent> contents = entry.getContents();
            System.out.println("=========================================================================================");
            for (SyndContent content : contents) {
                System.out.println(content.getValue().substring(0,200));
            }

            if(contents.size() == 0){
                System.out.println("fds");
            }
            System.out.println("=========================================================================================");

            // 此标题所属的范畴
            List categoryList = entry.getCategories();
            System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            if (categoryList != null) {
                for (int m = 0; m < categoryList.size(); m++) {
                    SyndCategory category = (SyndCategory) categoryList.get(m);
                    System.out.println("此标题所属的范畴：" + category.getName());
                }
            }
            System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

            // 得到流媒体播放文件的信息列表
            List enclosureList = entry.getEnclosures();
            if (enclosureList != null) {
                for (int n = 0; n < enclosureList.size(); n++) {
                    SyndEnclosure enclosure = (SyndEnclosure) enclosureList.get(n);
                    System.out.println("流媒体播放文件：" + entry.getEnclosures());
                }
            }
        }

    } catch (Exception ex) {
        System.out.println("ERROR: " + ex.getMessage());
    }
    }
}

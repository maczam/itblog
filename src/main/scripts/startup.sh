#!/bin/sh
cd `pwd`;cd ..
ITBLOG_HOME=`pwd`


ITBLOG_CONF_DIR=$ITBLOG_HOME/conf
ITBLOG_LOG_DIR=$ITBLOG_HOME/logs
ERROR_OUT_FILE=$ITBLOG_LOG_DIR/error_out.log

#java opts
JAVA_MEM_OPTS="-Xms256m -Xmx256m -Xmn96m -Djava.awt.headless=true "
JAVA_GC_OPTS_LOG=" -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintHeapAtGC -Xloggc:$ITBLOG_LOG_DIR/gc.log "


for f in $ITBLOG_HOME/*.jar; do
   CLASSPATH=${CLASSPATH}:$f;
done

for f in $ITBLOG_HOME/lib/*.jar; do
  CLASSPATH=${CLASSPATH}:$f;
done

CLASSPATH=${CLASSPATH}:$ITBLOG_CONF_DIR
echo $CLASSPATH


MAIN_CLASS_NAME=me.itblog.ProMain

nohup java -DLOG_DIR=$ITBLOG_LOG_DIR $JAVA_MEM_OPTS $JAVA_GC_OPTS_LOG -classpath $CLASSPATH $MAIN_CLASS_NAME >/dev/null 2>$ERROR_OUT_FILE &
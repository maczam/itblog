package org.beetl.fn;

import com.rometools.utils.Strings;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/12/8.
 */
public class DateUtil {

    public DateUtil() {
    }

    private static final String DEFAULT_KEY = "yyyy-MM-dd HH:mm:ss";

    private ThreadLocal<Map<String, SimpleDateFormat>> threadlocal = new ThreadLocal<Map<String, SimpleDateFormat>>();

    /**
     * data 转化成xx 前
     *
     * @param data
     * @return
     */
    public String at(Object data) {
        if (Date.class.isAssignableFrom(data.getClass())) {
            return createAt((Date) data);
        } else if (data.getClass() == Long.class) {
            Date date = new Date((Long) data);
            return createAt(date);
        } else {
            return data.toString();
        }
    }

    /**
     * 格式化
     *
     * @param data
     * @param pattern
     * @return
     */
    public String to(Object data, String pattern) {
        String result = null;
        if (data == null) {
            result = null;
        } else {
            pattern = Strings.isBlank(pattern) ? DEFAULT_KEY : pattern;
            if (Date.class.isAssignableFrom(data.getClass())) {
                SimpleDateFormat sdf = getDateFormat(pattern);
                result = sdf.format((Date) data);
            } else if (data.getClass() == Long.class) {
                Date date = new Date((Long) data);
                SimpleDateFormat sdf = getDateFormat(pattern);
                result = sdf.format(date);
            } else {
                throw new RuntimeException("参数错误，输入为日期或者Long:" + data.getClass());
            }
        }
        return result;
    }


    private SimpleDateFormat getDateFormat(String pattern) {
        Map<String, SimpleDateFormat> map = null;
        if ((map = threadlocal.get()) == null) {
            /**
             * 初始化2个空间
             */
            map = new HashMap<String, SimpleDateFormat>(4, 0.65f);
            threadlocal.set(map);
        }
        SimpleDateFormat format = map.get(pattern);
        if (format == null) {
            format = new SimpleDateFormat(pattern);
            map.put(pattern, format);
        }
        return format;
    }


    public String createAt(Date date) {
        if (date == null)
            return "未知";
        long now = System.currentTimeMillis() / 1000;
        long t = date.getTime() / 1000;
        long diff = now - t;
        if (diff < 5) {
            return "刚刚";
        }
        if (diff < 60) {
            return diff + "秒前";
        }
        if (diff < 60 * 60) {
            return (diff / 60) + "分钟前";
        }
        if (diff < 24 * 60 * 60) {
            return (diff / 60 / 60) + "小时前";
        }
        if (diff < 30 * 24 * 60 * 60) {
            return (diff / 24 / 60 / 60) + "天前";
        }

        if (diff < 365 * 24 * 60 * 60) {
            return (diff / 30 / 24 / 60 / 60) + "个月前";
        }
        return to(date, DEFAULT_KEY);
    }
}

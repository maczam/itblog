package me.itblog.utils;

import org.nutz.json.Json;
import org.nutz.lang.Lang;
import org.nutz.resource.NutResource;
import org.nutz.resource.Scans;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015/10/26.
 */
public class Constant {

    public static final String author = "maczam";
    public static final String github = "https://github.com/maczam";

    public static int list_page_size = 10;
    public static int search_list_page_size = 30;
    public static int left_list_page_size = 10;
    public static int hot_list_page_size = 5;
    public static int hot_category_size = 30;

    static Map<String, Object> versionMap = null;
    public static int fetch_connect_time_out = 3 * 60 * 1000;
    public static int fetch_read_time_out = 3 * 60 * 1000;

    static {
        try {
            List<NutResource> nutResources = Scans.me().loadResource("^version.json$", "data/");
            if (!Lang.isEmpty(nutResources)) {
                NutResource nutResource = nutResources.get(0);
                versionMap = (Map<String, Object>) Json.fromJson(nutResource.getReader());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }

    /**
     * 获取版本信息
     *
     * @return
     */
    public static Map<String, Object> getVersionMap() {
        return versionMap;
    }

    /**
     * 获取当前版本
     *
     * @return
     */
    public static String getCurrentVersion() {
        return versionMap.get("currentVersion").toString();
    }

}

package me.itblog;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by Administrator on 2015/11/4.
 */
public class ProMain {

    private static Logger logger = LoggerFactory.getLogger(ProMain.class);

    public static void main(String[] args) throws Exception {
        Server server = new Server(80);
        WebAppContext webapp = new WebAppContext();

        webapp.setContextPath("/");
        String path = DevMain.class.getClassLoader().getResource(".").getPath();
        logger.info("xxxxxxxxxxxxxxx>>>>" + path);
        File file = new File(path, "webapp");
        logger.info("xxxxxxxxxxxxxxx>>>>" + file.getPath());
        webapp.setResourceBase(file.getPath());
        HandlerCollection handlers = new HandlerCollection();
        handlers.setHandlers(new Handler[]{webapp, new DefaultHandler()});

        GzipHandler gzip = new GzipHandler();
        handlers.setHandlers(new Handler[]{webapp, new DefaultHandler()});
        gzip.setHandler(handlers);
        server.setHandler(gzip);


        server.start();
        /// 修改模板路径
        server.join();
    }
}

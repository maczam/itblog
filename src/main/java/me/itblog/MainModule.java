package me.itblog;

import org.beetl.ext.nutz.BeetlViewMaker;
import org.nutz.mvc.annotation.*;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

/**
 * Created by Administrator on 2015/10/6.
 */
@Modules(scanPackage = true)
@IocBy(type = ComboIocProvider.class, args = {
        "*json", "ioc/", "*anno","me.itblog",
        "*org.nutz.integration.quartz.QuartzIocLoader",// 关联Quartz
         })
@ChainBy(args="mvc/mvc-chain.json")
@Views(value = {BeetlViewMaker.class})
@SetupBy(MainSetup.class)
@Ok("json:full")
@Fail("jsp:jsp.500")
public class MainModule {
}

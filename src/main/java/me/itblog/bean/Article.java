package me.itblog.bean;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ManyMany;
import org.nutz.dao.entity.annotation.One;
import org.nutz.dao.entity.annotation.Table;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/10/18.
 */
@Table("t_article")
public class Article extends BasePojo {

    @Column
    @ColDefine(type = ColType.VARCHAR, width = 128)
    private String title;

    @Column("author_id")
    private String authorId;

    @One(target = AuthorProfile.class, field = "authorId")
    private AuthorProfile author;

    @Column
    @ColDefine(type = ColType.VARCHAR, width = 256)
    private String link;

    @Column
    @ColDefine(type = ColType.VARCHAR, width = 500)
    private String description;

    @Column("published_date")
    private Date publishedDate;

    @Column("author_name")
    private String authorName;

    @Column
    @ColDefine(type = ColType.TEXT)
    private String contents;

    @Column
    private String logoPicPath;

    @ManyMany(from = "article_id", relation = "t_article_category", target = Category.class, to = "category_id")
    List<Category> categoryList;

    @Column
    @ColDefine(type = ColType.VARCHAR, width = 512)
    private String categorys;

    @Column
    private long views;

    @Column
    protected boolean top;

    @Column
    protected boolean locked;

    /**
     *  是否将description 最为内容显示
     */
    @Column("show_description")
    @ColDefine(width = 1)
    private int showDescription;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AuthorProfile getAuthor() {
        return author;
    }

    public void setAuthor(AuthorProfile author) {
        this.author = author;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getLogoPicPath() {
        return logoPicPath;
    }

    public void setLogoPicPath(String logoPicPath) {
        this.logoPicPath = logoPicPath;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public String getCategorys() {
        return categorys;
    }

    public void setCategorys(String categorys) {
        this.categorys = categorys;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public boolean isTop() {
        return top;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getId (){
        return id;
    }

    public int getShowDescription() {
        return showDescription;
    }

    public void setShowDescription(int showDescription) {
        this.showDescription = showDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        return !(id != null ? !id.equals(article.id) : article.id != null);

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (publishedDate != null ? publishedDate.hashCode() : 0);
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (contents != null ? contents.hashCode() : 0);
        result = 31 * result + (logoPicPath != null ? logoPicPath.hashCode() : 0);
        result = 31 * result + (categoryList != null ? categoryList.hashCode() : 0);
        result = 31 * result + (categorys != null ? categorys.hashCode() : 0);
        result = 31 * result + (int) (views ^ (views >>> 32));
        result = 31 * result + (top ? 1 : 0);
        result = 31 * result + (locked ? 1 : 0);
        return result;
    }
}

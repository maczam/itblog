package me.itblog.bean;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.EL;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Prev;
import org.nutz.lang.random.R;

import java.util.Date;

/**
 * Created by maczam on 2015/10/9.
 */
public abstract class BasePojo implements java.io.Serializable {

    @Column("id")
    @Prev(els = @EL("$me.uuid()"))
    @Name
    protected String id;

    @Column("create_time")
    @Prev(els = @EL("$me.now()"))
    protected long createTime;

    @Column("update_time")
    @Prev(els = @EL("$me.now()"))
    protected long updateTime;


    public Date now() {
        return new Date();
    }

    public String uuid() {
        return R.UU32().toLowerCase();
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
}

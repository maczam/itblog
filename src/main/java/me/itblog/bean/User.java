package me.itblog.bean;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.One;
import org.nutz.dao.entity.annotation.Table;

/**
 * Created by infi.he on 2015/10/23.
 */
@Table("t_user")
public class User extends BasePojo {
    @Id
    protected int id;
    @Name
    @Column
    public String name;
    @Column("passwd")
    @ColDefine(width = 128)
    public String password;
    @Column
    protected String salt;
    @Column
    private boolean locked;

    @One(target = AuthorProfile.class, field = "id", key = "id")
    protected AuthorProfile profile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public AuthorProfile getProfile() {
        return profile;
    }

    public void setProfile(AuthorProfile profile) {
        this.profile = profile;
    }
}

package me.itblog.bean;

/**
 * Created by infi.he on 2015/12/23.
 */
public class LuceneSearchResult {
    private String id;
    private String highTitle;
    private String highContext;

    public LuceneSearchResult(String id, String highTitle,String highContext) {
        this.id = id;
        this.highTitle = highTitle;
        this.highContext = highContext;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHighTitle() {
        return highTitle;
    }

    public void setHighTitle(String highTitle) {
        this.highTitle = highTitle;
    }

    public String getHighContext() {
        return highContext;
    }

    public void setHighContext(String highContext) {
        this.highContext = highContext;
    }
}

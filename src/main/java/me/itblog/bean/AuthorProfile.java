package me.itblog.bean;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ManyMany;
import org.nutz.dao.entity.annotation.Readonly;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.json.JsonField;

import java.util.List;

/**
 * Created by Administrator on 2015/10/20.
 */
@Table("t_author_profile")
public class AuthorProfile extends BasePojo {

    @Column
    @ColDefine(type = ColType.VARCHAR, width = 128)
    private String categorys;

    @ManyMany(from = "author_id", relation = "t_author_category", target = Category.class, to = "category_id")
    List<Category> categoryList;

    @Column
    private String url;

    @Column
    private String feed;

    @Column
    private String name;

    @Column("fetch_type")
    @ColDefine(width = 2)
    private int fetchType;

    @Column
    @ColDefine(type = ColType.VARCHAR, width = 500)
    private String description;

    /**
     * 头像的byte数据
     */
    @Column
    @JsonField(ignore = true)
    protected byte[] avatar;

    @Column("article_count")
    @Readonly
    private long articleCount;

    /**
     * 1 :删除
     * 2 :不删除
     */
    @Column("is_delete")
    @ColDefine(width = 1)
    private int isDelete;

    /**
     *  是否将description 最为内容显示
     */
    @Column("show_description")
    @ColDefine(width = 1)
    private int showDescription;


    public String getCategorys() {
        return categorys;
    }

    public void setCategorys(String categorys) {
        this.categorys = categorys;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFetchType() {
        return fetchType;
    }

    public void setFetchType(int fetchType) {
        this.fetchType = fetchType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public long getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(long articleCount) {
        this.articleCount = articleCount;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public int getShowDescription() {
        return showDescription;
    }

    public void setShowDescription(int showDescription) {
        this.showDescription = showDescription;
    }
}

package me.itblog.bean;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Index;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

/**
 * Created by Administrator on 2015/10/9.
 */
@Table("wx_weixin")
public class WeiXin extends BasePojo {

    @Name
    @Column("id")
    private String id;

    //微信昵称
    @Column("name")
    private String name;

    @Index(name = "wx_weixin_open_id", fields = "open_id")
    @Column("open_id")
    private String openId;

    //微信号
    @Column("weixin_no")
    private String weixinNo;

    //功能介绍
    @Column("description")
    @ColDefine(type = ColType.VARCHAR, width = 512)
    private String description;

    //别的描述
    @Column("other")
    private String other;

    //认证公司
    @Column("company")
    private String company;

    @Column("article_ext_id")
    private String articleExtId;

    @Column("sougou_details_url")
    @ColDefine(type = ColType.VARCHAR, width = 256)
    private String sougouDetailsUrl;

    @Column("sougou_article_url")
    @ColDefine(type = ColType.VARCHAR, width = 256)
    private String sougouArticleUrl;

    @Column("sougou_logo_url")
    @ColDefine(type = ColType.VARCHAR, width = 256)
    private String sougouLogoUrl;

    @Column("sougou_qrcode_url")
    @ColDefine(type = ColType.VARCHAR, width = 256)
    private String sougouQrcodeUrl;

    @Column("logo_url")
    @ColDefine(type = ColType.VARCHAR, width = 256)
    private String logoUrl;

    @Column("qrcode_url")
    @ColDefine(type = ColType.VARCHAR, width = 256)
    private String qrcodeUrl;


    @Column("province")
    private Integer province;

    @Column("city")
    @ColDefine(width = 2)
    private Integer city;

    // 分类
    @Column("type_id")
    @ColDefine(width = 2)
    private Integer typeId;

    // 0 为认证  1认证通过  2 认证不通过
    @Column("status")
    @ColDefine(width = 2)
    private Integer status;

    //0 无人认领  1 已经认领
    @Column("identify")
    @ColDefine(width = 2)
    private Integer identify;


    public void init() {
        this.identify = 0;
        this.status = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getWeixinNo() {
        return weixinNo;
    }

    public void setWeixinNo(String weixinNo) {
        this.weixinNo = weixinNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getArticleExtId() {
        return articleExtId;
    }

    public void setArticleExtId(String articleExtId) {
        this.articleExtId = articleExtId;
    }

    public String getSougouDetailsUrl() {
        return sougouDetailsUrl;
    }

    public void setSougouDetailsUrl(String sougouDetailsUrl) {
        this.sougouDetailsUrl = sougouDetailsUrl;
    }

    public String getSougouArticleUrl() {
        return sougouArticleUrl;
    }

    public void setSougouArticleUrl(String sougouArticleUrl) {
        this.sougouArticleUrl = sougouArticleUrl;
    }

    public String getSougouLogoUrl() {
        return sougouLogoUrl;
    }

    public void setSougouLogoUrl(String sougouLogoUrl) {
        this.sougouLogoUrl = sougouLogoUrl;
    }

    public String getSougouQrcodeUrl() {
        return sougouQrcodeUrl;
    }

    public void setSougouQrcodeUrl(String sougouQrcodeUrl) {
        this.sougouQrcodeUrl = sougouQrcodeUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getQrcodeUrl() {
        return qrcodeUrl;
    }

    public void setQrcodeUrl(String qrcodeUrl) {
        this.qrcodeUrl = qrcodeUrl;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdentify() {
        return identify;
    }

    public void setIdentify(Integer identify) {
        this.identify = identify;
    }
}

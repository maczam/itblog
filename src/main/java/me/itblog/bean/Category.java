package me.itblog.bean;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ManyMany;
import org.nutz.dao.entity.annotation.Table;

import java.util.List;

/**
 * Created by Administrator on 2015/10/18.
 */
@Table("t_category")
public class Category extends BasePojo {

    @ManyMany(from = "category_id", relation = "t_article_category", target = Article.class, to = "article_id")
    List<Article> articleList;

    @Column
    private String name;

    @Column
    private long cnt;

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCnt() {
        return cnt;
    }

    public void setCnt(long cnt) {
        this.cnt = cnt;
    }

    public String getId(){
        return id;
    }

}

package me.itblog;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.webapp.WebAppContext;


/**
 * Created by Administrator on 2015/10/6.
 */
public class DevMain {

    public static void main(String[] args) throws Exception {

        String path = DevMain.class.getClassLoader().getResource(".").getPath();
        System.out.println("xxxxxxxxxxxxxxx>>>>" + path);
        System.out.println(path);

        Server server = new Server(8081);
        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        webapp.setResourceBase(path);
        webapp.setDefaultsDescriptor("jettywebdefault.xml");
//        HandlerCollection handlers = new HandlerCollection();

//        GzipHandler gzip = new GzipHandler();
//        handlers.setHandlers(new Handler[]{webapp, new DefaultHandler()});
//        gzip.setHandler(handlers);
//        server.setHandler(gzip);

//        handlers.setHandlers(new Handler[]{webapp, new DefaultHandler()});
        server.setHandler(webapp);
        server.start();
        /// 修改模板路径
        server.join();
    }
}

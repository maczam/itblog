package me.itblog;


import me.itblog.bean.Article;
import me.itblog.bean.AuthorProfile;
import me.itblog.bean.Category;
import me.itblog.bean.User;
import me.itblog.services.UserService;
import me.itblog.utils.Constant;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.integration.quartz.NutQuartzCronJobFactory;
import org.nutz.ioc.Ioc;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;
import org.quartz.Scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by maczam on 2015/10/9.
 */
public class MainSetup implements Setup {
    private static final Log log = Logs.get();

    public void init(NutConfig conf) {

        log.info("------------------------------------------");
        log.info("版本信息  :" + Constant.getCurrentVersion());
        log.info("开发者    :" + Constant.author);
        log.info("github地址:" + Constant.github);
        log.info("------------------------------------------");


        Ioc ioc = conf.getIoc();
        Dao dao = ioc.get(Dao.class);

        Daos.createTablesInPackage(dao, "me.itblog.bean", false);

//         获取NutQuartzCronJobFactory从而触发计划任务的初始化与启动
        ioc.get(NutQuartzCronJobFactory.class);

        // 初始化默认根用户
        User admin = dao.fetch(User.class, "admin");
        if (admin == null) {
            UserService us = ioc.get(UserService.class);
            us.add("admin", "123456");
        }


//        EmailService emailService = ioc.get(EmailService.class);
//        emailService.send("ydhexin@163.com","xxx","fd");

//        TestJob testJob = ioc.get(TestJob.class);
//        testJob.test();

    }

    /**
     * 初始化部分站点数据
     *
     * @param dao
     */
    private void initAuthorData(Dao dao) {

//        http://counter.cnblogs.com/blog/rss/4812461
        insertAuthorProfile(dao, "董的博客", "关注大规模数据处理，包括Hadoop，YARN，MapReduce，Spark，Mesos等",
                "http://dongxicheng.org/feed/", "http://dongxicheng.org/", "大数据");

    }

    private void insertAuthorProfile(Dao dao, String name, String description, String feed, String url, String categoryString) {
        AuthorProfile authorProfile = new AuthorProfile();
        authorProfile.setName(name);
        authorProfile.setDescription(description);
        authorProfile.setFeed(feed);
        authorProfile.setUrl(url);
        ///
        Category category = dao.fetch(Category.class, Cnd.where("name", "=", categoryString));
        authorProfile.setCategorys(category.getId() + "|" + category.getName());

        authorProfile.setCategoryList(Arrays.asList(category));

        dao.insert(authorProfile);
        dao.insertRelation(authorProfile, "categoryList");
    }


    /**
     * 初始化部分测试数据
     */
    private void initCategoryData(Dao dao) {
        List<String> ategorys = new ArrayList<String>();
        ategorys.add("移动应用");
        ategorys.add("前端开发");
        ategorys.add("服务端开发/管理");
        ategorys.add("产品/交互设计");
        ategorys.add("云计算");
        ategorys.add("大数据");
        ategorys.add("创业");
        ategorys.add("运维管理");

        for (String ategory : ategorys) {
            Category category = new Category();
            category.setName(ategory);
            dao.insert(category);
        }
    }

    public void destroy(NutConfig conf) {
        // 解决quartz有时候无法停止的问题
        try {
            conf.getIoc().get(Scheduler.class).shutdown(true);
        } catch (Exception e) {
        }
    }
}

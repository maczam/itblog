package me.itblog.mvc.modules.admin;

import me.itblog.bean.Category;
import me.itblog.mvc.filter.AdminAuthcActionFilter;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Administrator on 2015/11/7.
 */
@IocBean()
@At("/admin/category")
@Filters(@By(type = AdminAuthcActionFilter.class))
public class CategoryModule {
    private Logger logger = LoggerFactory.getLogger(CategoryModule.class);

    @Inject
    Dao dao;

    @At("/list")
//    @Ok("beetl:/index.btl")
    public List<Category> user() throws Exception {
        logger.info("CategoryModule >>> list");
        List<Category> query1 = dao.query(Category.class, null);
        return query1;
    }

    @At("/query")
//    @Ok("beetl:/index.btl")
    public List<Category> query(@Param("q")String q) throws Exception {
        logger.info("CategoryModule >>> list");
        logger.info("q >>> list");
        List<Category> query1 = dao.query(Category.class, Cnd.where(" name ", "like", "%" + q + "%"));
        return query1;
    }

}

package me.itblog.mvc.modules.admin;


import me.itblog.bean.AuthorProfile;
import me.itblog.bean.Category;
import me.itblog.mvc.filter.AdminAuthcActionFilter;
import me.itblog.utils.Constant;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.QueryResult;
import org.nutz.dao.Sqls;
import org.nutz.dao.entity.Entity;
import org.nutz.dao.pager.Pager;
import org.nutz.dao.sql.Sql;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.mvc.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Administrator on 2015/10/22.
 */
@IocBean()
@At("/admin/author")
@Filters(@By(type = AdminAuthcActionFilter.class))
public class AdminAuthorModule {

    private Logger logger = LoggerFactory.getLogger(AdminAuthorModule.class);

    @Inject
    Dao dao;

    @At("/list")
    public QueryResult user(@Param("page") int page, @Param("pageSize") int pageSize) throws Exception {
        logger.info("AdminModule >>> index");
        int currPage = page > 0 ? page : 1;
        pageSize = pageSize == 0 || pageSize > Constant.list_page_size ? Constant.list_page_size : pageSize;
        Pager pager = dao.createPager(page, pageSize);
        Entity<AuthorProfile> entity = dao.getEntity(AuthorProfile.class);
        Sql sql = Sqls.create("SELECT author.* ,count(1) article_count from t_author_profile author LEFT JOIN t_article ar on author.id = ar.author_id GROUP BY author.id ORDER BY author.create_time desc");
        sql.setCallback(Sqls.callback.entities()).setEntity(entity).setPager(pager);
        List<AuthorProfile> list = dao.execute(sql).getList(AuthorProfile.class);

        int count = dao.count(AuthorProfile.class, null);
        pager.setRecordCount(count);
        return new QueryResult(list, pager);

    }

    @At("/save")
    public String save(@Param("..") AuthorProfile authorProfile) throws Exception {
        logger.info("AdminModule >>> save");
        if (Strings.isBlank(authorProfile.getId())) {
            AuthorProfile fetch = dao.fetch(AuthorProfile.class, Cnd.where("url", "=", authorProfile.getUrl()));
            if (fetch == null) {
                dao.insert(authorProfile);
            } else {
                fetch.setName(authorProfile.getName());
                fetch.setCategorys(authorProfile.getCategorys());
                fetch.setFeed(authorProfile.getFeed());
                fetch.setDescription(authorProfile.getDescription());
                dao.update(fetch, "name|categorys|feed|description");
            }
        } else {
            dao.update(authorProfile, "name|categorys|feed|description|url");
        }
        return "";
    }

    @At("/fetch")
    public AuthorProfile fetch(@Param("id") String id) throws Exception {
        logger.info("AdminModule >>> fetch");
        AuthorProfile fetch = dao.fetch(AuthorProfile.class, Cnd.where("id", "=", id));
        return fetch;
    }

    @At("/delete")
    public String delete(@Param("id") String id) throws Exception {
        logger.info("AdminModule >>> delete");
        //TODO 删除额外数据 护着逻辑删除
        Sql sql = Sqls.create("delete from $author_table where id  = @id");
        sql.vars().set("author_table", dao.getEntity(AuthorProfile.class).getTableName());
        sql.params().set("id", id);
        dao.execute(sql);
        return "";
    }

    @At("/query")
//    @Ok("beetl:/index.btl")
    public List<AuthorProfile> query(@Param("q")String q) throws Exception {
        logger.info("AdminAuthorModule >>> list");
        logger.info("q >>> list");
        List<AuthorProfile> query1 = dao.query(AuthorProfile.class, Cnd.where(" name ", "like", "%" + q + "%"));
        return query1;
    }
}

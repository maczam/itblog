package me.itblog.mvc.processor;

import me.itblog.services.MenuService;
import me.itblog.utils.Constant;
import org.nutz.lang.Stopwatch;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.ActionContext;
import org.nutz.mvc.ActionInfo;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.impl.processor.AbstractProcessor;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by infi.he on 2015/12/1.
 */
public class LogTimeProcessor extends AbstractProcessor {

    private static final Log log = Logs.get();

    public LogTimeProcessor() {
    }

    @Override
    public void init(NutConfig config, ActionInfo ai) throws Throwable {
        MenuService.me().init();
    }

    @Override
    public void process(ActionContext ac) throws Throwable {
        Stopwatch sw = Stopwatch.begin();
        try {
            ac.getRequest().setAttribute("menuList",MenuService.me().getMenuList());
            ac.getRequest().setAttribute("currentVersion", Constant.getCurrentVersion());
            doNext(ac);
        } finally {
            sw.stop();
            if (log.isDebugEnabled()) {
                HttpServletRequest req = ac.getRequest();
                log.debugf("[%-4s]URI=%s %sms", req.getMethod(), req.getRequestURI(), sw.getDuration());
            }
        }
    }
}

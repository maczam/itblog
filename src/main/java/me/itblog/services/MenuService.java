package me.itblog.services;

import org.nutz.json.Json;
import org.nutz.lang.Lang;
import org.nutz.resource.NutResource;
import org.nutz.resource.Scans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015/12/11.
 */
public class MenuService {
    private static MenuService menuService = new MenuService();
    private boolean inited = false;

    List<Map<String, Object>> menuList = new ArrayList<>();


    public static MenuService me() {
        return menuService;
    }

    public List<Map<String, Object>> getMenuList() {
        init();
        return menuList;
    }

    public void init() {
        synchronized (this) {
            if (!inited) {
                try {
                    List<NutResource> nutResources = Scans.me().loadResource("^menu.json$", "mvc/");
                    if (!Lang.isEmpty(nutResources)) {
                        NutResource nutResource = nutResources.get(0);
                        List list = (List) Json.fromJson(nutResource.getReader());
                        for (Object o : list) {
                            Map menuItem = (Map) o;
                            List<Map> tagsList = (List) menuItem.get("tags");
                            List<String> tagIds = new ArrayList<>();
                            for (Map map : tagsList) {
                                tagIds.add(map.get("id").toString());
                            }
                            menuItem.put("tagIds", tagIds.toArray(new String[tagIds.size()]));
                            menuList.add(menuItem);
                        }
                        System.out.println(menuList);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                inited = true;
            }
        }
    }
}

define(['jquery', 'http', 'kit', 'select2'], function ($, http, kit) {
    $.fn.select2.defaults.set("theme", "bootstrap");

    function bind_table_event() {
        //编辑
        $('.edit-author-btn').unbind('click').on('click', function () {
            clear_author_modal_dialog();
            var id = $(this).data('id');
            console.log(id)
            http.get('/admin/author/fetch?id=' + id)
                .then(function (data) {
                    fill_author_modal_dialog(data);
                    $('#modal_demo_1').modal();
                });
        });

        //删除
        $('.del-author-btn').unbind('click').on('click', function () {
            var result = confirm('是否删除！');
            if(result){
                console.log($(this).data('id'))
                http.post('/admin/author/delete', {
                    id: $(this).data('id')
                }).then(function () {
                    load_author_table();
                })
            }
        });
    };

    /**
     * 获取数据
     */
    var load_author_table = function () {
        var pageSize = $('.author-table-page-size').val();
        var page = $('.pagination-panel-input').val();
        var url = '/admin/author/list?pageSize=' + pageSize + '&page=' + page;
        http.get(url)
            .done(function (data) {
                var table = [];
                $.each(data.list, function (i, v) {
                    var tr = [];
                    if (i % 2 == 0) {
                        tr.push('<tr role="row" class="odd">');
                    } else {
                        tr.push('<tr role="row" class="even">');
                    }
                    tr.push('<td>' + (i + 1) + '</td>');
                    tr.push('<td>' + v.name + '</td>');
                    tr.push('<td>' + v.url + '</td>');
                    tr.push('<td>' + v.feed + '</td>');
                    tr.push('<td>' + v.description + '</td>');
                    tr.push('<td>' + kit.formatLongDate16(v.createTime) + '</td>');
                    tr.push('<td>' + v.articleCount + '</td>');
                    tr.push('<td>' + v.showDescription + '</td>');
                    tr.push('<td>');
                    tr.push('<span><button data-id="' + v.id + '"  data-toggle="modal" class="btn btn-sm btn-outline grey-salsa edit-author-btn"><i class="fa fa-edit"></i>修改</button></span>');
                    tr.push('<span><button  data-id="' + v.id + '" data-toggle="modal" class="btn btn-sm btn-outline grey-salsa del-author-btn"><i class="fa fa-trash"></i>删除</button></span>');
                    tr.push('</td>');
                    tr.push('</tr>');
                    table.push(tr.join(''));
                });
                $('.table-responsive table tbody').empty().html(table.join(''));

                //页码相关
                $('#sample_editable_1_length  .total-count').text(data.pager.recordCount);
                $('.dataTables_paginate  .pagination-panel-total').text(data.pager.pageCount);
                //翻页
                if (page <= 1) {
                    $('.dataTables_paginate .prev').attr('disabled', true);
                } else {
                    $('.dataTables_paginate .prev').attr('disabled', false);
                }

                //翻页
                if (page >= data.pager.pageCount) {
                    $('.dataTables_paginate .next').attr('disabled', true);
                } else {
                    $('.dataTables_paginate .next').attr('disabled', false);
                }
                bind_table_event();
            })
            .fail(function () {
                console.log("---fail-----")
            });
    };


    var clear_author_modal_dialog = function () {
        $('#author_id').val('');
        $('#author_name').val('');
        $('#author_url').val('');
        $('#author_feed').val('');
        $('#author_description').val('');
        $('#fetch_type').val('');
        $('#show_description').val('');
        $("#author_categorys-multi-append").select2(http.select2('/admin/category/query', {
            processResults: function (data) {
                var tmpdata = [];
                $.each(data, function (i, v) {
                    v.id = v.id + "|" + v.name;
                    v.text = v.name;
                    tmpdata.push(v);
                });
                return {
                    results: tmpdata
                };
            }
        })).val(null).trigger("change");


        $("#author-save-btn").unbind('click').on('click', function () {
            var author_id = $(this).data('id');
            var author_name = $('#author_name').val();
            var author_url = $('#author_url').val();
            var author_feed = $('#author_feed').val();
            var author_description = $('#author_description').val();
            var fetch_type = $('#fetch_type').val();
            var show_description = $('#show_description').val();
            var author_categorys = $("#author_categorys-multi-append").val().join(";");

            var author = {
                id: author_id,
                name: author_name,
                url: author_url,
                feed: author_feed,
                description: author_description,
                categorys: author_categorys,
                fetchType: fetch_type,
                showDescription:show_description
            };

            console.log(JSON.stringify(author));
            http.post("/admin/author/save", author)
                .done(function () {
                    load_author_table();
                })
                .fail(function () {
                    console.log("---fail-----")
                });
        });
    };

    var fill_author_modal_dialog = function (author) {
        $('#author_id').val(author.id);
        $('#author_name').val(author.name);
        $('#author_url').val(author.url);
        $('#author_feed').val(author.feed);
        $('#author_description').val(author.description);
        $('#fetch_type').val(author.fetchType);
        $('#show_description').val(author.showDescription);
        var $select2 = $("#author_categorys-multi-append");
        var categorys = author.categorys ? author.categorys.split(';') : [];
        if (categorys.length > 0) {
            $.each(categorys, function (i, v) {
                var $option = $("<option selected></option>").val(categorys).text(v.split('|')[1]);
                $select2.append($option).trigger('change');
            });
        }
    };

    var bind_event = function () {
        // 新建 弹出事件
        $('#sample_editable_1_new').unbind('click').on('click', function () {
            clear_author_modal_dialog();
            $('#modal_demo_1').modal();
        });

        //table 底部事件
        $('.dataTables_paginate .prev').unbind('click').on('click', function () {
            $('.pagination-panel-input').val(parseInt($('.pagination-panel-input').val()) - 1);
            load_author_table()
        });
        $('.dataTables_paginate .next').unbind('click').on('click', function () {
            $('.pagination-panel-input').val(parseInt($('.pagination-panel-input').val()) + 1);
            load_author_table()
        });
        $('.author-table-page-size').unbind('change').on('change', function () {
            load_author_table()
        });
        $('.pagination-panel-input').unbind('keypress').on('keypress', function (event) {
            if (event.keyCode == "13") {
                load_author_table()
            }
        });
    }
    return {
        startup: function () {
            bind_event();
            load_author_table()
        }
    };
});


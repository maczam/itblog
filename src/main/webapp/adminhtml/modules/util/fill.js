define(['jquery','http',
        '../author','../home','../changepwd','../druid','../article'],
    function($,http,
             addauthor,home,changepwd,druid,article){
    return {
        fillup:function(name){
            var url = "./modules/" + name + ".html", menuId = 'menu-' + name;
            http.getHtml(url).then(function(result){
                console.log("name>>>>"+name);
                $('li.nav-item').removeClass('open');
                $('li.'+menuId).addClass('open');

                $("div.page-content-wrapper").children("div.page-content").remove();
                $("div.page-content-wrapper").append(result);
                switch(name){
                    case "author":
                        addauthor.startup();
                        break;
                    case "home":
                        home.startup();
                        break;
                    case "changepwd":
                        changepwd.startup();
                    case "druid":
                        druid.startup();
                    case "article":
                        article.startup();
                }
            },function(err){});
        }
    };
});
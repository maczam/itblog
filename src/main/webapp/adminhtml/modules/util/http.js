define(['jquery'],function($){
    var baseUrl = '';
    //var baseUrl = 'http://localhost:8081';
    //var baseUrl = 'http://180.76.146.214:80';
    return (function(){
        this.post = function(url,body){
            return $.ajax({
                type: "POST",
                url: baseUrl+url,
                data: body
            });
        };

        this.getHtml = function(url){
            return $.ajax({
                type: "GET",
                url: url,
            });
        };
        this.get = function(url){
            return $.ajax({
                type: "GET",
                url: baseUrl+url,
            });
        };

        this.do = function(url,bd){
            if(typeof(bd) == "funciton") bd = new db();
            return $.ajax({
                type: bd?"POST":"GET",
                url: baseUrl+url,
                data: bd
            });
        };

        this.select2 = function(url,conf){
            return {
                ajax: {
                    url: baseUrl+url,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: conf.processResults,
                    cache: true
                },
                minimumInputLength: 1
            }
        }
        return this;
    }());
});
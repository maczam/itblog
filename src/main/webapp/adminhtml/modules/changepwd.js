define(['jquery', 'http', 'kit'], function ($, http, kit) {
    return {
        startup: function () {
            $('#changepwd-btn').unbind('click').on('click', function () {
                var oldPwd = $('#old-pwd').val();
                var newPwd1 = $('#new-pwd1').val();
                var newPwd2 = $('#new-pwd2').val();
                if (oldPwd && newPwd1 && newPwd1 == newPwd2) {
                    http.post('/admin/changepwd', {
                        oldPwd: kit.md5(oldPwd),
                        newPwd: kit.md5(newPwd1)
                    }).then(function (data) {
                        console.log('xxxx');
                        console.log(data)
                    });
                } else {
                    alert("两次密码不一致！");

                }
            })
        }
    };
});

define(['jquery', 'http', 'kit', 'ueditor'], function ($, http, kit, ueditor) {

    var init_event = function () {
        $("#article_categorys").select2(http.select2('/admin/category/query', {
            processResults: function (data) {
                var tmpdata = [];
                $.each(data, function (i, v) {
                    v.id = v.id + "|" + v.name;
                    v.text = v.name;
                    tmpdata.push(v);
                });
                return {
                    results: tmpdata
                };
            }
        })).val(null).trigger("change");


        $("#article_author").select2(http.select2('/admin/author/query', {
            processResults: function (data) {
                var tmpdata = [];
                $.each(data, function (i, v) {
                    v.id = v.id + "|" + v.name;
                    v.text = v.name;
                    tmpdata.push(v);
                });
                return {
                    results: tmpdata
                };
            }
        })).val(null).trigger("change");
    };

    return {
        startup: function () {
            var ue = UE.getEditor('myEditor');
            $('#save-article-btn').unbind('click').on('click', function () {
                var context = ue.getContent();
                var article_title = $('#article_title').val();
                var article_author = $('#article_author').val();
                var author_categorys = $("#article_categorys").val().join(";");
                var article = {
                    context:context,
                    title:article_title,
                    author:article_author,
                    categorys:author_categorys
                };

                console.log(JSON.stringify(article));
                http.post("/admin/article/save", article)
                    .done(function () {
                        window.location.hash = '/home';
                    })
                    .fail(function () {
                        console.log("---fail-----")
                    });
            });
            init_event();
        }
    };
});
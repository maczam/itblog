require.config({
    urlArgs: "rnd=" + (new Date()).getTime(),
    paths: {
        'jquery': '../assets/global/plugins/jquery.min',
        'jquery-blockui': '../assets/global/plugins/jquery.blockui.min',
        'bootstrap': '../assets/global/plugins/bootstrap/js/bootstrap.min',
        'select2': '../assets/global/plugins/select2/js/select2.min',
        'ueditor': '../assets/global/plugins/ueditor/ueditor.all.min',
        'ueditor-config': '../assets/global/plugins/ueditor/ueditor.config',
        'http': './modules/util/http',
        'kit': './modules/util/kit'
    },
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'select2': {
            deps: ['jquery']
        },
        'jquery-blockui':{
            deps: ['jquery']
        },
        'ueditor':{
            deps:['ueditor-config']
        }
    }
});

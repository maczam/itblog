require(['./config'], function () {
    require(['jquery'], function ($) {
        require(['./modules/util/fill', 'bootstrap'], function (fill) {
            window.onhashchange = function () {
                var tag = window.location.hash.replace('#/', '') || 'home';
                fill.fillup(tag);
            };
            window.onhashchange();
        });
    });
});